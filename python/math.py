
# // basics
print(pow(2,2))
print(pow(2,0.5))
print(abs(-15))
print(round(pow(2,0.5)*100))
print(round(pow(2,0.5),2))

# // constants
from math import e, pi
print(round(pi,2))

# // trigonometry
from math import cos, acos, sin, asin, tan, atan, degrees, radians
print(sin(1))

# // Logarithm
from math import log, log10, log2


