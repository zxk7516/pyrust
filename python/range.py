import collections

Point = collections.namedtuple('point', 'x y')
p = Point(1,y=3)
print(p)

l = []
for i in range(10):
    l.append(i)

l2 = [i for i in range(10)]
l3 = [i*i for i in range(10)]

l4 = [i*i for i in range(5,10,2)]
print(l,l2,l3,l4)

for i, item in enumerate([1,2,3,2]):
    continue
    # print(i, item)

print(f'{l} \n{l2}')
print('{} \n{}'.format(l2,l3))
print('{x} \n{y}'.format(x=l3,y=l4))

Person = collections.namedtuple('Person', 'name height')
person = Person('Jean-Luc', 187)
print( f'{person.height:10}' )

name = 'tom'
print(  f'{name:<10}'  ) # 'tom       '
print(  f'{name:>10}'  ) # '       tom'
print(  f'{name:^10}'  ) # '    tom   '
print(  f'{name:->10}' ) # '-------tom'
print(  f'{name:>0}'   ) # 'tom'