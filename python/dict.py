import collections

d1 = {'name':"hello", 'age': 18};
print(d1.keys(), d1.values(),d1.items())

print(d1.get('height', 175), d1);
print(d1.setdefault('height', 175), d1);


# // default dict values func
d2 = collections.defaultdict(int)
d3 = collections.defaultdict(set)
d4 = collections.defaultdict(str)
d5 = collections.defaultdict(list)
d6 = collections.defaultdict(lambda: "hello")
print(d2[0],d3[0],d4[0],d5[0],d6[0])

# // filter
print({k:v for k, v in d1.items() if k in ['name','height']})

dict(['age','name'])

