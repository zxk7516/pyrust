import itertools
import functools;

list_a = [1,2,3,4];
list_b = [9,8,7,6];

sum_of_each = [sum(pari) for pari in zip(list_a,list_b)];
print(sum_of_each);


zipped = zip(reversed(list_a),list_b)
# print(list_a,list_b,list(zipped));
flatted = list(itertools.chain.from_iterable(zipped))
print(flatted);

# // zip 是一个生产者，使用过一次 (sonsume) 之后再次使用为空
zipped = zip(reversed(list_a),list_b)

list_b.extend(list_a);
sort_by_second = sorted(zipped, key= lambda el: el[1]);
print(sort_by_second);

# // 将字符串转换为字符数组
str = "hello";
print(list(str));

productions = functools.reduce(lambda out, x: out*x, [1,2,3])
print(productions);

# // list 去重
no_duplicated = list(dict.fromkeys(list("hello")))
print(no_duplicated);

# // indexOf
index = list("hello").index('o');
print("`hello` index of 'o' is ", index);

# // stack
list = [5,7,8,2]
list.insert(1,6);print(list);
print(list.pop(), list);
print(list.pop(1), list);
print(list.remove(5), list);
print(list.clear(), list);



