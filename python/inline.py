a = lambda a,b: a+b
print(a(1,2))

_list = [i+1 for i in range(10)]         # [1, 2, ..., 10]
_set  = {i for i in range(10) if i > 5}  # {6, 7, ..., 9}
_dict = {i: i*2 for i in range(10)}      # {0: 0, 1: 2, ..., 9: 18}
_iter = (x+5 for x in range(10))         # (5, 6, ..., 14)

print(_iter)

from urllib.parse import quote, quote_plus, unquote, unquote_plus
