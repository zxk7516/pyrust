#![allow(dead_code)]
fn f1() -> i32 {
    42
}

fn main() {
    // println!("{}", f1());
    // list();
    // dict();
    // str();
    // char();
    // counter();
    // slice();
    // set();
    range();
}

fn char() {}

fn range() {
    let mut l = vec![];
    for i in 5..10 {
        l.push(i);
    }
    let _: Vec<_> = (5..10).collect();
    let l2: Vec<_> = (5..10).collect();
    let l3: Vec<_> = (5..10).map(|i| i * i).collect();
    let l4: Vec<_> = (5..10).step_by(2).map(|i| i * i).collect();
    println!("{:?}\n{:?}\n{:?}\n{:?}", l, l2, l3, l4);
}

fn set() {
    use std::collections::HashSet;
    let mut set = HashSet::new();
    set.insert(1);
    set.insert(3);
    set.insert(7);
    println!("{:?}", set);

    let v = vec![1, 2, 3];

    let set2: HashSet<_> = v.into_iter().collect();
    println!(
        "{:?}",
        set.symmetric_difference(&set2).collect::<HashSet<_>>()
    );
    println!("{:?}", set.difference(&set2).collect::<HashSet<_>>());
    let unioned: HashSet<_> = set.union(&set2).cloned().collect();
    println!("{}", set.is_subset(&unioned));

    let joined: HashSet<_> = set.intersection(&set2).cloned().collect();
    println!("{}", set.is_superset(&joined));

    set.clear();
    println!("{:?}", set);
}

fn slice() {
    let s = &mut [2, 3, 5, 7, 9, 11, 13, 17, 19, 23, 29, 31, 35];
    println!("{}", s.len());
    assert!(!s.is_empty() && !s.first().is_none());

    if let Some(n) = s.first() {
        println!("Got first {}", n);
    }

    if let Some((two, _odd)) = s.split_first() {
        println!("{:?}", _odd);
        assert_eq!(two, &2);
    }

    if let Some(last) = s.last_mut() {
        *last = 37;
    }

    s.iter();
}

fn counter() {
    extern crate counter;
    use counter::Counter;
    let colors = vec!["blue", "red", "blue", "yellow", "blue", "red"];
    let mut counter = colors.iter().collect::<Counter<_>>();

    let colors2 = vec!["white", "black", "yellow"];
    counter.update(colors2.iter());
    println!("{:?}", counter);
}
fn str() {
    let s = "     22sfd fdkf jdflsjf lsjflsd flds fdf d>>><<<".to_string();
    let ltgt: &[_] = &['>', '<'];
    println!("{}", s.trim_matches(ltgt));
    println!(
        "{}",
        s.trim_matches(|c: char| c.is_whitespace() || c.is_numeric())
    );
    if s.starts_with(" ") || s.ends_with(" ") {
        println!("trimed: {}", s.trim());
    }

    let splited = s
        .split(|c: char| c.is_whitespace())
        .filter(|ref s| s.len() > 0)
        .collect::<Vec<&str>>();
    println!("splited {:?}", splited);

    let joined = splited.join("-");
    println!("{}", joined);

    if s.find("fd").is_some() {
        let s1 = s.replace("fd", "<FD>");
        println!("{}", s1);
    }

    let num = "12345a";
    let n: i32 = num.parse().unwrap_or_else(|e: std::num::ParseIntError| {
        println!("error: {:?}", e);
        5
    });
    println!("{}", n);

    extern crate textwrap;
    use textwrap::{fill, wrap};
    let s = "asdfjlsafsaf
asdfsafdsaf
asfdasf
";
    println!("{:?}", wrap(s, 10));
    println!("{}", fill(s, 10));
}

fn dict() {
    use std::collections::HashMap;
    let mut s1 = HashMap::new();
    s1.insert("name", "hello".to_owned());
    s1.insert("age", 18.to_string());
    println!("{}", s1.get("name").unwrap_or(&"".to_owned()));
}

fn list() {
    let mut v1 = vec![3, 2, 1];
    v1.push(4);
    v1.sort();

    println!("{:?}", v1);

    let v2 = vec![45, 23];
    v1.extend(v2.iter());

    println!("{:?}", v1);

    let mut v3_shifted = vec![90, 78];

    v1.append(&mut v3_shifted);

    println!("{:?} <- {:?}", v1, v3_shifted);

    v1.sort();
    v1.reverse();
    println!("{:?}", v1);

    println!("sum: {}", v1.iter().fold(0, |acc, v| acc + v));

    let a = vec![1, 2, 3];
    let mut b = vec![9, 8, 7];
    // elementwise_sum  = [sum(pair) for pair in zip(list_a, list_b)]
    let ab: Vec<i32> = a.iter().zip(b.iter()).map(|(ia, ib)| ia + ib).collect();
    println!("{:?}", ab);

    // let mut a_b: Vec<(i32,i32)> = a.into_iter().zip(b.into_iter()).collect();
    {
        let mut a_b: Vec<(&i32, &i32)> = a.iter().zip(b.iter()).collect();
        a_b.sort_by(|(_a1, b1), (_a2, b2)| b1.cmp(b2));
        println!("{:?}", a_b);
    }
    b[0] = 10;
}
